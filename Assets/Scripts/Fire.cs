﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public Transform startSplot;
    public Transform stopSplot;

    public float speed= 5;
     float timeToChange=2f;
    public float maxTimeToChange = 3f;
    bool changeSide;


    private void Update()
    {

        if (changeSide)
            transform.position -= (startSplot.position - stopSplot.position)* Time.deltaTime;
        else if(!changeSide) 
            transform.position -= -(startSplot.position - stopSplot.position) * Time.deltaTime;

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FireSpot")
        {
            Debug.Log("sd");
            changeSide = !changeSide;
        }

        if (other.tag == "Player")
        {
            Debug.Log("sd");
            other.GetComponent<Player2>().KillPlayer();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "FireSpot")
        {
            Debug.Log("sd");
            changeSide = !changeSide;
        }

        if (collision.tag == "Player")
        {
            Debug.Log("sd");
            collision.GetComponent<Player2>().KillPlayer();
        }
    }
}
