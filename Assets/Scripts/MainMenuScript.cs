﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Text highscoreLevel1Text;
    public Canvas mainCanvas;
    public Canvas plotCanvas;

    // Use this for initialization
    void Awake()
    {
        //highscoreLevel1Text.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel1");
    }

    // Use this for initialization
    void Start()
    {
        mainCanvas.enabled = true;
        plotCanvas.enabled = false;
        //highscoreLevel1Text.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel1");

    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator StartGame(string levelName)
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene(levelName);
    }

    public void onButtonPressed()
    {
        StartCoroutine(StartGame("CampaignLevel1"));
    }

    public void onCampaignButtonPressed()
    {
        mainCanvas.enabled = false;
        plotCanvas.enabled = true;
    }

    public void onEndlessButtonPressed()
    {
        int randomIndex = Random.Range(0, 3);
        if (randomIndex == 0)
        {
            StartCoroutine(StartGame("EndlessLevel1"));
        }
        else if (randomIndex == 1)
        {
            StartCoroutine(StartGame("EndlessLevel2"));
        }
        else if (randomIndex == 2)
        {
            StartCoroutine(StartGame("EndlessLevel3"));
        }
    }
}
