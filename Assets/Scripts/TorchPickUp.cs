﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchPickUp : MonoBehaviour
{
    AudioSource _as;
    Light _light;

    float boostLightValue = 3;
    float torchSpeedFade = 20f;
    float torchSpeedIntens = 10f;


    private void Start()
    {
        _as = GetComponent<AudioSource>();
        _light = GetComponent<Light>();
    }

    IEnumerator DecreaseLight()
    {
        _as.Play();
        Destroy(gameObject, _as.clip.length);
        while (true)
        {
            yield return null;
            _light.range -= torchSpeedFade * Time.deltaTime;
            _light.intensity -= torchSpeedIntens * Time.deltaTime;
            if (_light.range <= 0) break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Player>().IncreaseTorchRange(boostLightValue);
            StartCoroutine(DecreaseLight());
        }
    }

}
