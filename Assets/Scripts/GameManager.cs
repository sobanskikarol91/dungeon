﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    float timeAfterDie = 2f;
    public static GameManager instance;
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        else Destroy(this);
    }

    public void KeyPickedUp()
    {
        // open the door in SpawnManager
    }

    public void Death()
    {
        SceneManager.LoadScene("Level1");
    }

    public void WinLvl()
    {
        SceneManager.LoadScene("Level1");
    }
}