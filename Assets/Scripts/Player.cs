﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : Character
{
    float speed = 250f;
    Transform _transform;
    Rigidbody2D _rb2d;
    public Torch _torch;


    public bool isAlive = true;
    public bool godMode;
    public Sprite turnedFront;
    public Sprite turnedRight;
    public Sprite turnedLeft;
    public SpriteRenderer spriteRend;
    private Vector2 startTorchPosition;
    private bool turned;
    public Canvas nextLevelCanvas;
    public Canvas gameOverCanvas;
    public AudioClip deathClip;
    public GameObject torchPrefab;
    private int torchFireUp = 1;
    private int speedUp = 1;
    private int godModeUp = 1;
    public Canvas gameCanvas;
    public Image godImage;
    public Image speedImage;
    public Image torchImage;

    void Start()
    {
        base.Start();
        _as = GetComponent<AudioSource>();
        _transform = GetComponent<Transform>();
        _rb2d = GetComponent<Rigidbody2D>();
        nextLevelCanvas.enabled = false;
        gameOverCanvas.enabled = false;
    }

    private IEnumerator godModeRoutine()
    {
        godMode = true;
        spriteRend.color = Color.white;
        godImage.enabled = false;
        yield return new WaitForSeconds(1f);
        spriteRend.color = Color.gray;
        godMode = false;
    }

    private IEnumerator speedUpRoutine()
    {
        speed += 250.0f;
        speedImage.enabled = false;
        yield return new WaitForSeconds(1f);
        speed -= 250.0f;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && torchFireUp > 0)
        {
            torchImage.enabled = false;
            torchPrefab.GetComponent<Light>().range = Mathf.Min(10.0f, 5.0f + torchPrefab.GetComponent<Light>().range);
            torchFireUp--;
        }
        if (Input.GetMouseButtonDown(1) && speedUp > 0)
        {
            speedUp--;
            StartCoroutine(speedUpRoutine());
        }
        if (Input.GetMouseButtonDown(2) && godModeUp > 0)
        {
            godModeUp--;
            StartCoroutine(godModeRoutine());
        }

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x == 0 && y == 0) return;

        if(!_as.isPlaying)
        _as.Play();

        Vector3 offset = new Vector2(x, y);

        if (x > 0 && y > 0)
            offset = offset.normalized;

        if (x == 0)
        {
            spriteRend.sprite = turnedFront;
        }
        if (x > 0)
        {
            spriteRend.sprite = turnedRight;
        }
        else if (x < 0)
        {
            spriteRend.sprite = turnedLeft;
        }

        _rb2d.velocity = offset * speed * Time.deltaTime;
    }

    private IEnumerator EndGame()
    {
        _as.clip = deathClip;
        _as.Play();
        gameOverCanvas.enabled = true;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("MainMenu");
    }

    private IEnumerator NextLevel()
    {
        nextLevelCanvas.enabled = true;
        yield return new WaitForSeconds(1f);
        int randomIndex = Random.Range(0, 3);
        if (randomIndex == 0)
        {
            SceneManager.LoadScene("EndlessLevel1");
        }
        else if (randomIndex == 1)
        {
            SceneManager.LoadScene("EndlessLevel2");
        }
        else if (randomIndex == 2)
        {
            SceneManager.LoadScene("EndlessLevel3");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Find")
        {
           //collision.gameObject.SetActive(false);
        }
        else if (collision.tag == "Enemy" && isAlive && !godMode)
        {
            isAlive = false;
            StartCoroutine(EndGame());

        }
        else if (collision.tag == "Key")
        {
            SpawnMenager.instance.openTheDoor();

        }
        else if (collision.tag == "Exit")
        {
            StartCoroutine(NextLevel());
        }
    }

    public void IncreaseTorchRange(float value)
    {
        _torch.IncreaseTorchRange(value);
    }

    public void KillPlayer()
    {
        if (!godMode)
        {
            return;
        }
        speed = 0;
        _as.clip = deathClip;
        _as.Play();
        StartCoroutine(EndGame());
    }

}