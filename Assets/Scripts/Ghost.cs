﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : Character {

    public float moveSpeed;
    private float xStart;
    private float yStart;
    private bool inRange;
    bool isFroozen;
    public GameObject plejer;

    public bool playerIsDetected = false;

    // Use this for initialization
    void Start()
    {
        base.Start();
        xStart = _transform.position.x;
        yStart = _transform.position.y;
        inRange = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFroozen) return;

        if (!inRange)
        {
            float x = xStart - SpawnMenager.instance.player.transform.position.x;
            float y = yStart - SpawnMenager.instance.player.transform.position.y;
            if (Mathf.Abs(x * x - y * y) < 10f)
            {
                inRange = true;
            }
        }
        if (inRange)
        {
            float x = _transform.position.x - plejer.transform.position.x;
            float y = _transform.position.y - plejer.transform.position.y;
            float moveSpeedX = x / (Mathf.Abs(x) + Mathf.Abs(y));
            float moveSpeedY = y / (Mathf.Abs(x) + Mathf.Abs(y));
            _transform.Translate(moveSpeed * -moveSpeedX * Time.deltaTime, moveSpeed * -moveSpeedY * Time.deltaTime, 0);

        }
    }

    public IEnumerator FreezeEnemy(float freezeTime)
    {
        isFroozen = true;
        yield return new WaitForSeconds(freezeTime);
        isFroozen = false;
    }
}




