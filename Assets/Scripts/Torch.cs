﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour
{

    public float maxRange = 10f;
    float minRange =2.3f;

    public float maxIntensivity = 10f;
    float minIntensivity = 3f;

    [Range(0.1f, 10)]
    public float rangeSpeedChange = 0.3f;

    [Range(0.1f, 10)]
    public float intensivitySpeedChange = 0.1f;

    Light _light;

    public ParticleSystem smokeParticle;
    public ParticleSystem fireParticle;


    private void Start()
    {
        _light = GetComponent<Light>();
    }

    private void Update()
    {
        if (_light.range <= minRange || _light.intensity <= minIntensivity)
        {
            _light.range = minRange;
            //_light.intensity = minIntensivity;
            smokeParticle.enableEmission = false;
            fireParticle.enableEmission = false;
            return;
        }
        else
        {
            smokeParticle.enableEmission = true;
            fireParticle.enableEmission = true;
            _light.range -= Time.deltaTime * rangeSpeedChange;
        }
    }

    public void IncreaseTorchRange(float _value)
    {
        float newRange = _light.range + _value;

        if (newRange > maxRange) _light.range = maxRange;
        else _light.range = newRange;

    }
}
