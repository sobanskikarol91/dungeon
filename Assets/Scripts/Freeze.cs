﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freeze : MonoBehaviour
{
    AudioSource _as;
    float freezeTime = 2f;
    bool isPickUped;

    private void Start()
    {
        _as = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player") return;
        if (isPickUped) return;
        SpawnMenager.instance.FreezeEnemy(freezeTime);
        Destroy(gameObject);
    }
}
