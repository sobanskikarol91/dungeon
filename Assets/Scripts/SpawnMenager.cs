﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMenager : MonoBehaviour
{
    public static SpawnMenager instance;
    public List<GameObject> keyPrefab = new List<GameObject>();
    public List<GameObject> enemyPrefab = new List<GameObject>();
    public List<GameObject> findPrefab = new List<GameObject>();
    public List<GameObject> startDoorPrefab = new List<GameObject>();
    public List<GameObject> exitDoorPrefab = new List<GameObject>();
    public List<GameObject> startWallPrefab = new List<GameObject>();
    public List<GameObject> exitWallPrefab = new List<GameObject>();

    public GameObject player;
    public Sprite openDoorSprite;
    private int exitDoor;
    private List<Enemy> spawnedEnemies = new List<Enemy>();


    private void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        player.SetActive(true);
        int randomIndex;
        randomIndex = Random.Range(0, keyPrefab.Count);
        keyPrefab[randomIndex].gameObject.SetActive(true);
        randomIndex = Random.Range(0, findPrefab.Count);
        findPrefab[randomIndex].gameObject.SetActive(true);

        for (int i = 0; i < 3; i++)
        {
            randomIndex = Random.Range(0, enemyPrefab.Count);
            if (enemyPrefab[randomIndex].gameObject.activeInHierarchy == false)
            {
                enemyPrefab[randomIndex].gameObject.SetActive(true);
                spawnedEnemies.Add(enemyPrefab[randomIndex].GetComponent<Enemy>());
            }
            else
            {
                i--;
            }
        }
        randomIndex = Random.Range(0, startDoorPrefab.Count);
        for (int i = 0; i < startDoorPrefab.Count; i++)
        {
            if (i == randomIndex)
            {
                startDoorPrefab[i].gameObject.SetActive(true);
                player.transform.position = startDoorPrefab[i].transform.position;
                player.transform.Translate(1, 0, 0);
            }
            else
            {
                startWallPrefab[i].gameObject.SetActive(true);
            }
        }
        randomIndex = Random.Range(0, exitDoorPrefab.Count);
        for (int i = 0; i < exitDoorPrefab.Count; i++)
        {
            if (i == randomIndex)
            {
                exitDoorPrefab[i].gameObject.SetActive(true);
                exitDoor = i;
            }
            else
            {
                exitWallPrefab[i].gameObject.SetActive(true);
            }
        }
    }

    void Update()
    {

    }

    public void openTheDoor()
    {
        exitDoorPrefab[exitDoor].gameObject.GetComponent<SpriteRenderer>().sprite = openDoorSprite;
        exitDoorPrefab[exitDoor].gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }

   public void FreezeEnemy(float freezeTime)
    {
        foreach (var e in spawnedEnemies)
        {
            StartCoroutine(e.FreezeEnemy(freezeTime));
        }
    }
}
