﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    protected Transform _transform;
    protected AudioSource _as;

    float speed = 1f;


    virtual public void Start()
    {
        _transform = GetComponent<Transform>();
        _as = GetComponent<AudioSource>();
    }
}
