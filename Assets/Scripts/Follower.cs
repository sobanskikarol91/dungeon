﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform toFollow;


    private void Start()
    {
        StartCoroutine(IEStartFollow());
    }

    public IEnumerator IEStartFollow()
    {
        while (true)
        {
            yield return null;
            if (toFollow != null)
                transform.position = new Vector3(toFollow.position.x, toFollow.position.y, transform.position.z);
        }
    }

    public void StopFollow()
    {
        StopAllCoroutines();
    }
}
