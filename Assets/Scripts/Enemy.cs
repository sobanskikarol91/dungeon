﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{

    public float moveSpeed;
    private float xStart;
    private float yStart;
    private bool inRange;
    bool isFroozen;
    public Sprite turnedFront;
    public Sprite turnedRight;
    public Sprite turnedLeft;
    public bool  playerIsDetected=false;
    public GameObject plejer;

    // Use this for initialization
    void Start()
    {
        base.Start();
        xStart = _transform.position.x;
        yStart = _transform.position.y;
        inRange = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFroozen) return;
        //float x = _transform.position.x - SpawnMenager.instance.Player.transform.position.x;
        //float y = _transform.position.y - SpawnMenager.instance.Player.transform.position.y;
        if (!inRange)
        {
            //float x = xStart - SpawnMenager.instance.player.transform.position.x;
            //float y = yStart - SpawnMenager.instance.player.transform.position.y;
            float x = xStart - plejer.transform.position.x;
            float y = yStart - plejer.transform.position.y;
            if (Mathf.Abs(x * x - y * y) < 5f)
            {
                inRange = true;
            }
        }
        if (inRange)
        {
            float x = _transform.position.x - plejer.transform.position.x;
            float y = _transform.position.y - plejer.transform.position.y;
            float moveSpeedX = x / (Mathf.Abs(x) + Mathf.Abs(y));
            float moveSpeedY = y / (Mathf.Abs(x) + Mathf.Abs(y));
            _transform.Translate(moveSpeed * -moveSpeedX * Time.deltaTime, moveSpeed * -moveSpeedY * Time.deltaTime, 0);

            if (x == 0)
            {
                GetComponent<SpriteRenderer>().sprite = turnedFront;
            }
            else if (x < 0)
            {
                GetComponent<SpriteRenderer>().sprite = turnedRight;
            }
            else if (x > 0)
            {
                GetComponent<SpriteRenderer>().sprite = turnedLeft;
            }
        }
    }

   public IEnumerator FreezeEnemy(float freezeTime)
    {
        isFroozen = true;
        yield return  new WaitForSeconds(freezeTime);
        isFroozen = false;
    }
}
