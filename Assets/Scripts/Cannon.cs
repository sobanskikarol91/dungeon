﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
   public GameObject bullet;


    public void Fire()
    {
        Debug.Log("Fire");
        Instantiate(bullet, transform.position, Quaternion.identity);
    }
}
